<?php

namespace App\Http\Controllers;

use App\Bid;
use App\Blog;
use Illuminate\Http\Request;
use App\Buyer;
use App\Seller;
use App\Article;
use App\HomePageSetup;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Nexmo\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $checkPage = HomePageSetup::where('user_id',$user->id)->first();

        if($checkPage){
//echo $checkPage->homepage_link;exit;
            return redirect()->to($checkPage->homepage_link);

        }

        $id = $request->user()->id;
        $user=User::find($id);
        $user->onlineStatus=1;
        $user->save();
        $buyers = Buyer::orderBy('id', 'desc')->paginate(10);
        $sellers = Seller::orderBy('id', 'desc')->paginate(10);
        $articles = Article::orderBy('id', 'desc')->paginate(10);
        return view('home')
        ->withBuyers($buyers)
        ->withSellers($sellers)
        ->withArticles($articles)
        ->withId($id);
    }

    public function filter(Request $request){
       $keywords = $request->get('input_search');
       $type = $request->get('post_for');
       $sort = $request->get('sort');
       $min = $request->get('min');
       $max = $request->get('max');
       $user = $request->get('user');
       $online = $request->get('online');
       $mypost = $request->get('mypost');
       $lat = $request->get('lat');
       $lng = $request->get('lng');
       $miles = $request->get('miles');
        $result = array();
        $result_type = 0;
        if($type == "posts" or $type=="0"){
            $result_type = 0;
            $query = Blog::with('user')->leftJoin('users',"blog_posts.user_id","=","users.id");


            if($user != null){

                $query->where('user_id',"=", intval($user));
            }
            if($online != null) {
                $query
                    ->where("users.onlineStatus", "=", 1)
                ;
            }
            if ($lat != null){
//                $lat = 41.31;
//                $lng = -72.92;
                $query->whereRaw("
                       ST_Distance(
                            point(users.lng, users.alt),
                            point(?, ?)
                        ) * .000621371192 < ?
                    ", [
                                    $lng,
                                    $lat,
                    $miles
                                ]);

            }
            if($mypost){
                $query->where('user_id',"=", Auth()->user()->id);
            }
//            if(floatval($max)> 0){
//                $query->where('read_amount', "=<" , $max);
//            }
//
            if($keywords != null){
                $query->where('content','LIKE', '%'.$keywords.'%')
                    ->orWhere('heading', 'LIKE', '%'.$keywords.'%');
            }

            if($sort == "oldest"){
                $query->orderBy("blog_posts.created_at", "asc");
            }elseif ($sort == "newest"){
                $query->orderBy("blog_posts.created_at", "desc");
            }elseif ($sort == "high"){
                $query->orderBy("blog_posts.read_amount", "desc");
            }else{
                $query->orderBy("blog_posts.read_amount", "asc");

            }

            $blogPosts = $query
                ->select([
                    'blog_posts.*',
                    'users.name',
                    'users.email'
                ])
                ->get();
            foreach ($blogPosts as $post){
                $btn = null;
                if($post->read_amount == 0){
                    $btn = '<a href="/blod-details/' . $post->id .'" class="btn btn-primary">Details</a>';
                }else{
                    $btn = '<button type="button" onclick="showMsg('."'". $post->heading ."'".','.$post->read_amount. ','. $post->user_id.','. $post->id .')" class="btn btn-primary">Details</button>';
                }
                $p = [
                    "post_id" => $post->id,
                    'btn' => $btn,
                    'url' => '/blod-details/'.$post->id,
                    "user_name" => $post->user->name,
                    "user_pic" => $post->user->avatar,
                    "user_id" => $post->user_id,
                    "heading" => $post->heading,
                    "image" => "/blog/".$post->image,
                    'created_at' => $post->created_at,
                    "amount" =>$post->read_amount
                ];
                if(floatval($min)> 0 and $max==null){
                    if($post->read_amount > floatval($min))
                        array_push($result, $p);
                }elseif(floatval($max)> 0 and $min==null){
                    if($post->read_amount < floatval($max))
                        array_push($result, $p);
                }elseif (floatval($min)> 0 and floatval($max)> 0){
                    if($post->read_amount < floatval($max) and $post->read_amount > floatval($min) ){
                        array_push($result, $p);
                    }
                }else{
                    array_push($result, $p);
                }

            }
        }

        if( $type == "bids" or $type=="0"){
            $result_type = 0;
            $query = Bid::with('user', 'product')
                ->leftJoin('users',"bids.user_id","=","users.id")
                ->leftJoin('products',"bids.product_id","=","products.id");

            if($user != null){

                $query->where('user_id',"=", intval($user));
            }
            if($online != null) {
                $query
                    ->where("users.onlineStatus", "=", 1)
                ;
            }
            if ($lat != null){
//                $lat = 41.31;
//                $lng = -72.92;
                $query->whereRaw("
                       ST_Distance(
                            point(users.lng, users.alt),
                            point(?, ?)
                        ) * .000621371192 < ?
                    ", [
                    $lng,
                    $lat,
                    $miles
                ]);

            }
            if($mypost){
                $query->where('user_id',"=", Auth()->user()->id);
            }
            if($keywords != null){
                $query
                    ->where('description', 'LIKE', '%'.$keywords.'%')
                    ->orWhere('address', 'LIKE', '%'.$keywords.'%')
                    ->orWhere('phone', 'LIKE', '%'.$keywords.'%')
                    ->orWhere('country', 'LIKE', '%'.$keywords.'%')
                    ->orWhere('city', 'LIKE', '%'.$keywords.'%')
                    ->orWhere('products.name', 'LIKE', '%'.$keywords.'%');
            }
            if($sort == "oldest"){
                $query->orderBy("bids.created_at", "asc");
            }elseif ($sort == "newest"){
                $query->orderBy("bids.created_at", "desc");
            }elseif ($sort == "high"){
                $query->orderBy("bids.current_bid", "desc");
            }else{
                $query->orderBy("bids.current_bid", "asc");

            }

            $bids = $query->get();
            foreach ($bids as $bid){
                $btn = '<a href="/bids/buyers" class="btn btn-primary">Details</a>';
                $p = [
                    "post_id" => $bid->id,
                    'btn' => $btn,
                    'url' => '/bids/buyers',
                    "user_name" => $bid->user->name,
                    "user_pic" => $bid->user->avatar,
                    "user_id" => $bid->user_id,
                    "heading" => $bid->product->name,
                    "image" => "/buyer/".$bid->product->image,
                    'created_at' => $bid->created_at,
                    "amount" =>$bid->current_bid
                ];

                if(floatval($min)>= 0 and $max==null){
                    if($bid->current_bid >= floatval($min))
                        array_push($result, $p);
                }elseif(floatval($max)>= 0 and $min==null){
                    if($bid->current_bid <= floatval($max))
                        array_push($result, $p);
                }elseif (floatval($min)>= 0 and floatval($max)>= 0){
                    if($bid->current_bid <= floatval($max) and $bid->current_bid >= floatval($min) ){
                        array_push($result, $p);
                    }
                }
            }
        }
        $m = $result_type;
        if($sort == "oldest"){
            $this->array_sort_by_column($result, 'created_at');
        }elseif ($sort == "newest"){
            $this->array_sort_by_column($result, 'created_at',SORT_DESC);
        }elseif ($sort == "high"){
            $this->array_sort_by_column($result, 'amount', SORT_DESC);
        }else{
            $this->array_sort_by_column($result, 'amount');

        }


       return view('home',compact('result', $result_type));
    }

    function array_sort_by_column(&$array, $column, $direction = SORT_ASC) {
        $reference_array = array();

        foreach($array as $key => $row) {
            $reference_array[$key] = $row[$column];
        }

        array_multisort($reference_array, $direction, $array);
    }
}
