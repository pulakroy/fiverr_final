<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;

use App\Setting;

class SettingsController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
        return view('pages.settings.index', compact('setting'));
    }

    public function updateEnv(Request $request)
    {
        $setting = Setting::first();

        if (! $setting) {
            $setting = Setting::firstOrCreate([]);
        }
        
        try {
            $setting->update($request->except('_token'));
        } catch (\Exception $e) {
            Session::flash('failure', 'Server error: ' . $e->getMessage());
        }
        
        Session::flash('success', 'Settings updated');
        return redirect()->back();
    }

    protected function setEnvironmentValue(array $values)
    {
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);

        if (count($values) > 0) {
            foreach ($values as $envKey => $envValue) {

                $str .= "\n"; // In case the searched variable is in the last line without \n
                $keyPosition = strpos($str, "{$envKey}=");
                $endOfLinePosition = strpos($str, "\n", $keyPosition);
                $oldLine = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);

                // If key does not exist, add it
                if (!$keyPosition || !$endOfLinePosition || !$oldLine) {
                    $str .= "{$envKey}={$envValue}\n";
                } else {
                    $str = str_replace($oldLine, "{$envKey}={$envValue}", $str);
                }

            }
        }

        $str = substr($str, 0, -1);
        if (!file_put_contents($envFile, $str)) return false;
        return true;

    }
}
