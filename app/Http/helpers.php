<?php

function isFollowing($userId, $followableId)
{
    $followStatus = false;
    $isFollowing = (new \App\Follow())->where('user_id', $userId)->where('followable_id', $followableId)->first();
    if ($isFollowing)
    {
        if ($isFollowing->status == 1)
        {
            $followStatus = true;
        }
    }

    return $followStatus;

}

function userReview($userId, $reviewable_id)
{
    $reviewNumber = 0;
    $isReviewExist = (new \App\Review())->where('user_id', $userId)->where('reviewable_id', $reviewable_id)->first();
    if ($isReviewExist)
    {
        $reviewNumber = $isReviewExist->review_number;
    }

    return $reviewNumber;
}

function averageReview($userId)
{

    $averageReview = (new \App\Review())->where('reviewable_id', $userId)->avg('review_number');

    return number_format($averageReview, 1);
}
function totalReview($userId)
{


    $totalReview = (new \App\Review())->where('reviewable_id', $userId)->count();

    return $totalReview;
}


function isEventFavorited($eventId)
{
    $event = \App\Event::find($eventId);


    return $event->isFavorited();
}
function setSettings($settings)
{
    config( ['mail' => ['from' => ['address' => $settings->mail_username
        , 'name' => $settings->mail_username], 'host'=>'smtp.gmail.com', 
        'port'=>'587', 'username'=>$settings->mail_username,  
        'password'=>$settings->mail_password, 
        'encryption'=>'tls', 
        'driver'=>'smtp']]);




      config(['broadcasting'=>[ 'default' => 'pusher',    
        'connections' => [
    
            'pusher' => [
                'driver' => 'pusher',
                'key' => $settings->pusher_app_key,
                'secret' => $settings->pusher_app_secret,
                'app_id' => $settings->pusher_app_id,
                'options' => [
                    'cluster' => $settings->pusher_cluster,
                    'encrypted' => false,
                ],
            ],
    
            'redis' => [
                'driver' => 'redis',
                'connection' => 'default',
            ],
    
            'log' => [
                'driver' => 'log',
            ],
    
            'null' => [
                'driver' => 'null',
            ],
    
        ],]]);
}
?>


