@extends('layouts.app')

@section('content')
    <style type="text/css">
        .btn {
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: .25rem;
            transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .time-and-true {
            border: 1px solid #eee;
            text-align: center;
            margin-top: 8px;
            padding: 3px;
        }

        .middle-div {
            background: #eee;
            margin: 41px 1px 20px -6px;
            height: 119px;
            padding: 13px;
        }

        .time-left-div {
            background: #f9f7f7;
            padding: 5px;
            height: 45px;
        }

        .referral-div input {
            width: 20%;
            padding: 0 3px 3px 5px;
            margin: 0 0 0 52px;
        }

        .referral-div h6 {
            font-weight: 700;
            font-size: .99rem;
            display: inline;
            vertical-align: sub;
        }
        .fa-question-circle{
            vertical-align: sub;
        }

        h6 {
            font-size: .80rem;
        }

        h5 {
            font-size: .95rem;
        }

        .top-image {
            height: 50px;
            width: 50px;
            float: left;;
        }

        .product-image {
            height: 190px;
            width: 330px;
            box-shadow: 1px 1px 1px 1px #eee;

        }

        .want-to-buy {
            width: 98% !important;
            padding: 0;
            background: #ffffff;
            margin: 0;
        }

        .current-bid-input {
            display: inline;
            width: 15%;

        }

        .product-name {
            width: 100%;
            font-weight: 800;

            text-align: center;
        }

        .bid-box {
            box-shadow: 1px 1px 1px 1px #0b682c;
            padding: 12px;
            margin: 32px;
        }

        @media only screen and (max-width: 600px) {
            .product-image {
                height: 184px;
                /*margin: 0;*/
                width: 240px;
                /*border-radius: 9%;*/
                box-shadow: 1px 1px 1px 1px #eee;
            }

            .product-name {
                width: 100%;
                font-weight: 800;

                text-align: center;
            }

            .want-to-buy {
                width: 100% !important;
                padding: 0;
                background: #ffffff;
                margin: 0;
            }

            .middle-div h6 {
                margin-left: -22px;
            }

            .middle-div h4 {
                margin-left: -39px;
            }

            .float-left{
                margin-top: 35px;
            }

            .middle-div {
                background: #eee;
                margin: 23px -3px 26px -3px;;
                height: 119px;
                padding: 13px;
            }

            .referral-div input {
                width: 28%;
                padding: 1px 10px 2px 14px;
                margin: 5px 3px 42px 25px;
            }

            .bid-box {
                box-shadow: 1px 1px 1px 1px #0b682c;
                padding: 3px;
                margin: 4px;
            }
        }

    </style>


    @include('partials._user-profile')
    <div class="container" style="width: 64%;">
        <div class="card">
            <div class="card-header">
                <img src="{{asset('img/logo.png')}}" class="top-image">
                <h3 style="display: inline">Buyer</h3>
                {{--<a href="{{url('/home')}}" style="float:right"><i class="fas fa-times"></i></a>--}}

            </div>
            <div class="card-body">

                @php
                    $u=Auth::user()->id;
                    $bid=App\Bid::where('user_id',$u)->get();
                    //$product=App\product::where('id',$bid->product_id)->first();
                    // print_r($bid);
                    // exit();

                @endphp

                @if($bid)
                    @foreach($bid as $bid)
                        @php
                            $product=App\product::where('id',$bid->product_id)->first();
                        @endphp
                        @if($ifany > 0 )
                            <a href="{{url('/edit-bid-form/'.$bid->id)}}">
                                @else
                                    <a href="#">
                                        @endif
                                        <div class="col-md-4 float-left card {{--bid-box--}}">
                                            <img src="{{asset('uploads/buyer/'.$product->image)}}"
                                                 class="product-image">
                                            <div class="dropdown">
                                                <button class="dropdown-toggle want-to-buy" type="button"
                                                        id="dropdownMenu2" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    Want to buy
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                    <button class="dropdown-item" type="button">Item 1</button>
                                                    <button class="dropdown-item" type="button">Item action</button>
                                                    <button class="dropdown-item" type="button">Item</button>
                                                </div>
                                            </div>
                                            <div class="time-and-true">
                                                <i type="text" class="product-name">{{$product->name}}</i>

                                            </div>

                                            <div class="row middle-div">
                                                <div class="col-6">
                                                    <h6>Current bid </h6>
                                                </div>
                                                <div class="col-6">
                                                    <h4>US $</h4>
                                                    <input type="number" value="{{$bid->current_bid}}"
                                                           name="current_bid" class="current-bid-input"><br>

                                                </div>
                                                <input type="checkbox" value="{{$bid->auto_order}}"
                                                       name="auto_order"><span>Auto-Order</span>
                                            </div>
                                            {{--<div class="middle-div">--}}

                                            <br>
                                            <div class="time-left-div">
                                                <h5> Time left </h5>
                                                <div class="remaining-time"><span><i class="fas fa-hourglass-half"></i></span>
                                                    2houres 2 min
                                                </div>
                                            </div>

                                            {{--</div>--}}
                                            <div class="referral-div">
                                                <div style="display: inline"><input type="text"
                                                                                    value="{{$bid->referral}}"
                                                                                    name="referral"></div>
                                                <h6 style="display: inline">% Referral</h6>
                                                <i class="fas fa-question-circle"></i>
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach
                                @endif

                                <a href="{{url('/buyer_bid_form')}}" class="btn float-right want-to-buy"> <i
                                            class="fas fa-plus"></i> Want to buy </a>
                            </a>
            </div>

        </div>
    </div>
@endsection

@section('extra-JS')

@endsection
