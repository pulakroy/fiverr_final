@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div id="columns">
                    @foreach ($posts as $post)
                        @if ($post -> post_type == "buyer")
                            <?php
                            $buyer = App\Buyer::where('id',$post->post_id)->get()->first();
                            if(!empty($seller)){
                            ?>
                            <figure class="share-save-icon">
                                <img class="share-save-icon-buyr w-4"  style="" src="{{asset('img/' . 'referrer.png')}}">
                                <img id="{{$buyer->id}}" data-value="{{$id}}" data-value1="buyer" class="share-save-icon-buyr w-4 m-2-45"  style="" src="{{asset('img/' . 'save3.jpg')}}">
                                <a href="{{ route('buyer.show', $buyer->id) }}">
                                    <img src="{{ asset('uploads/buyer/' . $buyer->buyer_featured_image) }}">
                                    <figcaption><strong>{{ $buyer->buyer_pro_title }}</strong></figcaption>
                                    <figcaption class="mt-2"><small class="text-muted">{{ date('M j, Y', strtotime($buyer->created_at)) }}</small></figcaption>
                                    <figcaption class="float-left"><small class="text-muted"><strong>Buy</strong></small></figcaption>
                                </a>
                            </figure>
                            <?php } ?>
                        @elseif($post -> post_type == "seller")
                            <?php
                            $seller = App\Seller::where('id',$post->post_id)->get()->first();
                            if(!empty($seller)){
                            ?>
                            <figure class="share-save-icon">
                                <img class="share-save-icon-buyr w-4" src="{{asset('img/' . 'referrer.png')}}">
                                <img id="{{$seller->id}}" data-value="{{$id}}" data-value1="seller" class="share-save-icon-buyr w-4 m-2-45"  style="" src="{{asset('img/' . 'save3.jpg')}}">
                                <a href="{{ route('seller.show', $seller->id) }}">
                                    <img src="{{ asset('uploads/seller/' . $seller->seller_featured_image) }}">
                                    <figcaption><strong>{{ $seller->seller_pro_title }}</strong></figcaption>
                                    <figcaption class="mt-2"><small class="text-muted">{{ date('M j, Y', strtotime($seller->created_at)) }}</small></figcaption>
                                    <figcaption class="float-left"><small class="text-muted"><strong>Sell</strong></small></figcaption>
                                </a>
                            </figure>
                            <?php } ?>
                        @else
                            <?php
                            $article = App\Article::where('id',$post->post_id)->get()->first();
                            if(!empty($article)){
                            ?>
                            <figure class="share-save-icon">
                                <img class="share-save-icon-buyr w-4" src="{{asset('img/' . 'referrer.png')}}">
                                <img id="{{$article->id}}" data-value="{{$id}}" data-value1="article" class="share-save-icon-buyr w-4 m-2-45"  style="" src="{{asset('img/' . 'save3.jpg')}}">
                                <a href="{{ route('article.show', $article->id) }}">
                                    <img src="{{ asset('uploads/article/' . $article->article_featured_image) }}">
                                    <figcaption><strong>{{ $article->article_title }}</strong></figcaption>
                                    <figcaption class="mt-2"><small class="text-muted">{{ date('M j, Y', strtotime($article->created_at)) }}</small></figcaption>
                                    <figcaption class="float-left"><small class="text-muted"><strong>Article</strong></small></figcaption>
                                </a>
                            </figure>
                            <?php } ?>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>

        <div class="row">
            @foreach ($favoriteEvents as $event)

                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-img-top">
                            <a href="{{ route('events.show', $event->id) }}">
                                <img src="{{ asset('uploads/event/' . $event->event_featured_image) }}" style="width: 100%;">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 p-0 pl-2">


                                    <h3>{{ $event->event_title }}</h3>

                                </div>
                            </div>
                            <hr id="event_view_border">

                            <div class="row">

                                <div class="col-md-7 p-0 pl-2">


                                    <label class="" for="" style="">Online</label><br>
                                    @if(empty($event->interested_in_event))
                                        0 interested
                                    @else
                                        {{ ($event->interested_in_event) }} interested
                                    @endif
                                    <br>
                                    @if(empty($event->going_in_event))
                                        0 going
                                    @else
                                        {{ ($event->going_in_event) }} going
                                    @endif
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4 ">


                                </div>
                            </div>
                            <div class="row" style="margin-top:10px">
                                <div class="col-md-6">
                                    <div class="text-muted">
                                        {{ $event->event_city }}

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-muted">

                                        {{ $event->event_country }}
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="col-md-12" style="margin-bottom:5px">
                            <hr id="event_view_border">
                            <button class="btn btn-default">
                                <a href="#" onclick="shareOnFb(  {{ $event->id }})"><i class="fas fa-share-square fa-1x"></i></a>
                            </button>
                            <a href="{{route('events.save', $event->id)}}">
                                <button class="btn btn-default">

                                    @if(isEventFavorited($event->id))
                                        <i class="fas fa-star"></i>
                                    @else

                                        <i class="far fa-star fa-1x"></i>
                                    @endif

                                </button>
                            </a>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
@section('extra-JS')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".share-save-icon-buyr").click(function()
        {
            var user_id = $(this).attr("data-value");
            var post_id =  $(this).attr("id");
            var post_type =  $(this).attr("data-value1");
            var status = 0;
            $.ajax({
                url: "SavePost",
                type: "POST",
                data: {user_id : user_id, post_id:post_id, post_type:post_type,status:status},
                dataType: "JSON",
                success: function (data) {
                    console.log(data);
                    location.reload();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        });
    </script>

    <script>
        function shareOnFb(eventId) {
            var url = "{{ route('events.show', ['EVENT_ID' => "EVENT_ID"]) }}".replace("EVENT_ID", eventId);
            FB.ui({
                method: 'share',
                href: url,
                app_id: "2223725464559470"
            }, function (response) {
            });
        }
    </script>
@endsection
