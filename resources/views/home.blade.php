@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div id="columns">
                @isset($buyers)
                    @foreach ($buyers as $buyer)
                        <figure class="share-save-icon">
                            <img class="share-save-icon-buyr w-4"  style="" src="{{asset('img/' . 'referrer.png')}}">

                            <img id="{{$buyer->id}}" data-value="{{$id}}" data-value1="buyer" data-value-2="blue" class="share-save-icon-buyr w-4 m-2-45 blue <?php if($buyer->buyer_saved_status == 1){ ?>  hide  <?php } ?>"  style="" src="{{asset('img/' . 'save2.png')}}">
                            <img id="{{$buyer->id}}" data-value="{{$id}}" data-value1="buyer" data-value-2="yellow" class="share-save-icon-buyr w-4 m-2-45 yellow <?php if($buyer->buyer_saved_status == 0){ ?>  hide  <?php } ?>"  style="" src="{{asset('img/' . 'save3.jpg')}}">
                            <?php
                            if($buyer->buyer_saved_status != 1 && $buyer->buyer_saved_status != 0)
                            {
                            ?>
                            <img id="{{$buyer->id}}" data-value="{{$id}}" data-value1="buyer" data-value-2="blue" class="share-save-icon-buyr w-4 m-2-45 blue"  style="" src="{{asset('img/' . 'save2.png')}}">
                            <?php
                            }
                            ?>
                            <a href="{{ route('buyer.show', $buyer->id) }}">
                                <img src="{{ asset('uploads/buyer/' . $buyer->buyer_featured_image) }}">
                                <figcaption><strong>{{ $buyer->buyer_pro_title }}</strong></figcaption>
                                <figcaption class="mt-2"><small class="text-muted">{{ date('M j, Y', strtotime($buyer->created_at)) }}</small></figcaption>
                                <figcaption class="float-left"><small class="text-muted"><strong>Buy</strong></small></figcaption>
                            </a>
                        </figure>
                    @endforeach
                @endisset
                @isset($sellers)
                        @foreach ($sellers as $seller)
                            <figure class="share-save-icon">
                                <img class="share-save-icon-buyr w-4" src="{{asset('img/' . 'referrer.png')}}">
                                <img id="{{$seller->id}}" data-value="{{$id}}" data-value1="seller" data-value-2="blue" class="share-save-icon-buyr w-4 m-2-45 blue <?php if($seller->seller_saved_status == 1){ ?>  hide  <?php } ?>"  style="" src="{{asset('img/' . 'save2.png')}}">
                                <img id="{{$seller->id}}" data-value="{{$id}}" data-value1="seller" data-value-2="yellow" class="share-save-icon-buyr w-4 m-2-45 yellow <?php if($seller->seller_saved_status == 0){ ?>  hide  <?php } ?>"  style="" src="{{asset('img/' . 'save3.jpg')}}">
                                <?php
                                if($seller->seller_saved_status != 1 && $seller->seller_saved_status != 0)
                                {
                                ?>
                                <img id="{{$seller->id}}" data-value="{{$id}}" data-value1="seller" data-value-2="blue" class="share-save-icon-buyr w-4 m-2-45 blue"  style="" src="{{asset('img/' . 'save2.png')}}">
                                <?php
                                }
                                ?>
                                <a href="{{ route('seller.show', $seller->id) }}">
                                    <img src="{{ asset('uploads/seller/' . $seller->seller_featured_image) }}">
                                    <figcaption><strong>{{ $seller->seller_pro_title }}</strong></figcaption>
                                    <figcaption class="mt-2"><small class="text-muted">{{ date('M j, Y', strtotime($seller->created_at)) }}</small></figcaption>
                                    <figcaption class="float-left"><small class="text-muted"><strong>Sell</strong></small></figcaption>
                                </a>
                            </figure>
                        @endforeach
                @endisset
                    @isset($articles)
                        @foreach ($articles as $article)
                            <figure class="share-save-icon">
                                <img class="share-save-icon-buyr w-4" src="{{asset('img/' . 'referrer.png')}}">
                                <img id="{{$article->id}}" data-value="{{$id}}" data-value1="article" data-value-2="blue" class="share-save-icon-buyr w-4 m-2-45 blue <?php if($article->article_saved_status == 1){ ?>  hide  <?php } ?>"  style="" src="{{asset('img/' . 'save2.png')}}">
                                <img id="{{$article->id}}" data-value="{{$id}}" data-value1="article" data-value-2="yellow" class="share-save-icon-buyr w-4 m-2-45 yellow <?php if($article->article_saved_status == 0){ ?>  hide  <?php } ?>"  style="" src="{{asset('img/' . 'save3.jpg')}}">
                                <?php
                                if($article->article_saved_status != 1 && $article->article_saved_status != 0)
                                {
                                ?>
                                <img id="{{$article->id}}" data-value="{{$id}}" data-value1="article" data-value-2="blue" class="share-save-icon-buyr w-4 m-2-45 blue"  style="" src="{{asset('img/' . 'save2.png')}}">
                                <?php
                                }
                                ?>
                                <a href="{{ route('article.show', $article->id) }}">
                                    <img src="{{ asset('uploads/article/' . $article->article_featured_image) }}">
                                    <figcaption><strong>{{ $article->article_title }}</strong></figcaption>
                                    <figcaption class="mt-2"><small class="text-muted">{{ date('M j, Y', strtotime($article->created_at)) }}</small></figcaption>
                                    <figcaption class="float-left"><small class="text-muted"><strong>Article</strong></small></figcaption>
                                </a>
                            </figure>
                        @endforeach
                    @endisset



            </div>
        </div>
    </div>

    <div class="row">
        @isset($result)
            @foreach($result as $res)
                <div class="col-md-3    ">
                    <div class="card text-center card-width">
                        <div class="card-header filter">
                            <div class="row no-margin" style="width: 100%">
                                <div class="col-md-2" style="padding-right: 0; padding-left: 2px">
                                    <div class="img-container">
                                        <img width="100%" src="{{ url('/uploads/avatars').'/' . $res['user_pic'] }}">
                                    </div>
                                </div>
                                <div class="col-md-5" style="padding-left: 0;text-align: left">
                                    <div class="title-container ml-2"><span style="position: absolute;bottom: 0;">{{ $res['user_name'] }}</span></div>
                                </div>
                                <div class="col-md-5 align-self-end">
                                    <input id="ownRatingMobileCard" name="ownRating" class="rating rating-loading own-rating rating-xs ownRatingMobileCard"
                                           value="{{averageReview($res['user_id'])}}" style="padding-top: 8px;">

                                </div>
                            </div>

                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <img width="100%" src="{{url('/uploads') .$res['image']}}">
                                </div>
                            </div>
                            <h5 class="card-title">{{$res['heading']}}</h5>
                        </div>
                        <div class="card-footer text-muted">
                            {!!  $res['btn'] !!}
                        </div>
                    </div>
                </div>
            @endforeach
        @endisset


        {{--<div class="col-md-3">--}}
            {{--<div class="card text-center card-width">--}}
                {{--<div class="card-header">--}}
                    {{--<div class="row no-margin" style="width: 100%">--}}
                        {{--<div class="col-md-2" style="padding-right: 0; padding-left: 2px">--}}
                            {{--<div class="img-container">--}}
                                {{--<img width="100%" src="http://hi5working.test/uploads/avatars/1542355689.jpg">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-5" style="padding-left: 0;text-align: left">--}}
                            {{--<div class="title-container ml-2"><span style="position: absolute;bottom: 0;">Elu</span></div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-5 align-self-end">--}}
                            {{--<input id="ownRatingMobileCard" name="ownRating" class="rating rating-loading own-rating rating-xs ownRatingMobileCard"--}}
                                   {{--value="{{averageReview(Auth::user()->id)}}" style="padding-top: 8px;">--}}

                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}
                {{--<div class="card-body">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col">--}}
                            {{--<img width="100%" src="http://hi5working.test/uploads/avatars/1542355689.jpg">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<h5 class="card-title">Elu@Elu.com</h5>--}}
                {{--</div>--}}
                {{--<div class="card-footer text-muted">--}}
                    {{--<a href="#" class="btn btn-primary">Go somewhere</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}


    </div>
</div>
@endsection
@section('extra-JS')
<script>

    $( document ).ready(function() 
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
       $(".share-save-icon-buyr").click(function()
       {
            var user_id = $(this).attr("data-value");
            var post_id =  $(this).attr("id");
            var post_type =  $(this).attr("data-value1");
            var saved_cls = $(this).attr("data-value-2");
            var status = "";
            if(saved_cls == "blue"){
                $(this).addClass('hide');
                $(this).next().removeClass('hide');
                // $(".blue").addClass('hide');
                // $(".yellow").removeClass('hide');
                status = 1;
            }
            else
            {
                $(this).addClass('hide');
                $(this).prev().removeClass('hide');
                // $(".yellow").addClass('hide');
                // $(".blue").removeClass('hide');
                status = 0;
            }
           $.ajax({
                    url: "SavePost",
                    type: "POST",
                    data: {user_id : user_id, post_id:post_id, post_type:post_type,status:status},
                    dataType: "JSON",
                    success: function (data) {
                            console.log(data); 
                           
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            alert('Something went wrong');
                        }
                });
       });
    });

    function showMsg(heading, amount,owner_id,id){
        Swal.fire({
            title: 'Pay To Read?',
            text: "You have to pay $"+amount+ " to read!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: 'green',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Pay'
        }).then((result) => {
            if (result.value) {
                $.ajax({

                    url: '/blog/pay-to-read/'+amount+'/'+owner_id,
                    type: 'GET',

                    success: function (response) {

                        console.log(response);
                        Swal.fire(
                            'Payment Done!',
                            'You can now read blog.',
                            'success'
                        ).then((result) => {
                            if (result.value) {
                                window.location = '/blod-details/'+id;
                            }
                        });

                    }
                });
            }
        });
    }
</script>

@endsection
