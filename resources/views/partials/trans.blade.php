
<nav aria-label="Page navigation example">
    <ul class="pagination pagination-sm">
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous" id="first">
                <span aria-hidden="true">&laquo;</span>
                <span>First</span>
            </a>
        </li>
        <li class="page-item disabled">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&laquo;</span>
                <span>Previous</span>
            </a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span>Next</span>
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>
<hr>


    <!-- transaction heading row -->
    <div class="row">
        <div class="col-md-12">
            <table class="table wallet-transaction-table">
                <thead>
                <tr>
                    <th scope="col">Date <i class="fas fa-caret-down"></i></th>
                    <th scope="col">Transaction <i class="fas fa-caret-down"></i></th>
                    <th scope="col">Descriptions <i class="fas fa-caret-down"></i></th>
                    <th scope="col">Desposits/Credits <i class="fas fa-caret-down"></i></th>
                    <th scope="col">Withdrawals/Debits <i class="fas fa-caret-down"></i></th>
                    <th scope="col">Ending Daily Balance <i class="fas fa-caret-down"></i></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>


    </div>

    <!-- pending transaction row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card border-0 mb-0 p-0">
                <div class="card-header card-header pl-4 pb-0">
                    <p>
                        <small><strong>Pending Transactions</strong></small>
                    </p>
                </div>
                <div class="card-body">


                    @foreach($withdraw_requests as $key=>$value)
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-3">

                                <button class="deleteRecord text-secondary"
                                        style="float: left; padding-right: 5px; border: 0; margin-right: 5px;"
                                        data-id="{{ $value['id'] }}">
                                    <small><i class="fa fa-close action-icon"></i>
                                    </small>
                                </button>
                                @if($user_role_as_accountant == 9)
                                    <button class="acceptedWithdrawRequest text-secondary"
                                            style="float: left; padding-right: 5px; border: 0; margin-right: 5px;"
                                            data-id="{{ $value['id'] }}">
                                        <small>
                                            <i class="glyphicon glyphicon-ok action-icon"></i>
                                        </small>
                                    </button>
                                @endif

                                <a class="text-secondary someclass" href="#"
                                   role="button" aria-expanded="false"
                                   aria-controls="collapseDetails">
                                    <small><i onClick="matulpost({{$key + 1}})"
                                              class="fas fa-plus-circle" style="margin-right: 5px;"></i>{{$value['datwise']}}
                                    </small>
                                </a>
                            </div>

                            <div class="col-md-2 col-sm-2 col-2">
                                <a class="text-secondary" href="#">
                                    <small>{{@$value['transaction_id']}}</small>
                                </a>
                            </div>
                            <div class="col-md-3 col-sm-3 col-3">
                                <a class="text-secondary" href="#">
                                    <small>{{$value['description']}}</small>
                                </a>
                            </div>
                            <div class="col-md-1 col-sm-1 col-1">
                                <p class="text-secondary">
                                    <small><?php echo($value['type'] == 'cr' ? $value['amount'] : ''); ?></small>
                                </p>
                            </div>
                            <div class="col-md-1 col-sm-1 col-1">
                                <p class="text-secondary">
                                    <small><?php echo($value['type'] == 'db' ? $value['withdraw'] : ''); ?></small>
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div style="display:none"
                                     id="PendingDetails{{$key + 1}}">
                                    <div class="card ">
                                        <div class="card-header pt-4">
                                            <h6 class="card-title"><strong>Details</strong>
                                            </h6>
                                        </div>
                                        <div class="card-body">

                                            <?php echo $value['details']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>

        </div>
    </div>

    <!-- posted transaction row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card border-0 mb-0 p-0">
                <div class="card-header card-header pl-4 pb-0">
                    <p>
                        <small><strong>Posted Transactions</strong></small>
                    </p>
                </div>
                <div class="card-body">


                    @foreach($posted_transactions as $key=>$value)
                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-2">
                                <a class="text-secondary someclass" role="button"
                                   aria-expanded="false" aria-controls="collapseDetails">
                                    <small><i id="matulmyelement"
                                              onClick="matulpostdetail({{$key + 1}})"
                                              class="fas fa-plus-circle"></i> {{$value['datwise']}}
                                    </small>
                                </a>
                            </div>
                            <div class="col-md-2 col-sm-2 col-2">
                                <a class="text-secondary" href="#">
                                    <small>{{@$value['transaction_id']}}</small>
                                </a>
                            </div>
                            <div class="col-md-3 col-sm-3 col-3">
                                <a class="text-secondary" href="#">
                                    <small>{{$value['description']}}</small>
                                </a>
                            </div>
                            <div class="col-md-1 col-sm-1 col-1">
                                <p class="text-secondary">
                                    <small><?php echo($value['type'] == 'cr' ? $value['amount'] : ''); ?></small>
                                </p>
                            </div>
                            <div class="col-md-1 col-sm-1 col-1">
                                <p class="text-secondary">
                                    <small><?php echo($value['type'] == 'db' ? $value['withdraw'] : ''); ?></small>
                                </p>
                            </div>
                            <div class="col-md-2 col-sm-2 col-2">
                                <p class="text-secondary">
                                    <small>
                                        <?php

                                        echo $value['ending_daily_balance'];

                                        ?>
                                    </small>
                                </p>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div style="display:none" id="PostedDetails{{$key+1}}">
                                    <div class="card ">
                                        <div class="card-header pt-4">
                                            <h6 class="card-title"><strong>Details</strong></h6>
                                        </div>
                                        <div class="card-body">

                                            <?php echo $value['details']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>

<script type="text/javascript">
    $(document).on('click', 'button.deleteRecord', function(event){
        event.preventDefault();

        $("#loader").removeClass('hide');
        $(".main-c").addClass('hide');
        var id = $(this).data("id");

        $.ajax(
            {
                url: "cancelWithdrawRequest",
                type: "POST",
                data: {
                    "id": id,
                },
                dataType: "JSON",
                success: function (data)
                {
                    $("#loader").addClass('hide');
                    $(".main-c").removeClass('hide');
                    if(data['error'] == 0)
                    {

                        swal({
                            title: "You have canel withdraw request successfull!",
                            text: "",
                            icon: "success",
                            button: "OK",
                        });

                        window.location.reload();
                    }
                    else
                    {
                        swal({
                            title: "Sorry!! due to some reasons your request is not accepted. Please try again",
                            text: "",
                            icon: "error",
                            button: "OK",
                        });
                    }

                },
                error: function(XMLHttpRequest, textStatus, errorThrown)
                {
                    //alert(errorThrown);
                }
            });
    });

    $(document).on('click', 'button.acceptedWithdrawRequest', function(event){

        event.preventDefault();
        $("#loader").removeClass('hide');
        $(".main-c").addClass('hide');
        var id = $(this).data("id");

        $.ajax(
            {
                url: "approveWithdrawRequest",
                type: "POST",
                data: {
                    "id": id,
                },
                dataType: "JSON",
                success: function (data)
                {
                    $("#loader").addClass('hide');
                    $(".main-c").removeClass('hide');
                    if(data['error'] == 0)
                    {

                        swal({
                            title: "Your withdraw request is accepted. Thanks!",
                            text: "",
                            icon: "success",
                            button: "OK",
                        });

                        window.location.reload();
                    }
                    else
                    {
                        swal({
                            title: "Please try again later",
                            text: "",
                            icon: "error",
                            button: "OK",
                        });
                    }

                },
                error: function(XMLHttpRequest, textStatus, errorThrown)
                {
                    //alert(errorThrown);
                }
            });

    });
</script>